from django.contrib import admin
from .models import Reunion


@admin.register(Reunion)
class ReunionAdmin(admin.ModelAdmin):
	model = Reunion